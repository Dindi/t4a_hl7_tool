#!/usr/bin/env node
'use strict';
// load the Node.js TCP library
var net = require('net');
var PORT = 5000;
var ADDRESS = '';

var server = net.createServer(onClientConnected);
server.listen(PORT, ADDRESS);
function onClientConnected(socket) {
    //console.log(socket.remoteAddress:socket.remotePort);
    console.log("Connected Address => " + socket.remoteAddress + "Port => " + socket.remotePort);
    socket.destroy();
}

console.log("Server started on PORT : => " + ADDRESS + ": " + PORT);